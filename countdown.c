///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  ./countdown
//
// Result:
//   Counts the seconds since 7 am on Feb 22, 1980, and
//   then outputs the elapsed time in years, days, hours, minutes and seconds.
//   It uses a hardcoded date, and some rudimentary leap day checking
//
// Example:
//   Reference time:  Fri Feb 22 07:00:00 1980
//   Years: 42  Days: 2  Hours: 1  Minutes: 5  Seconds: 42
//   Years: 42  Days: 2  Hours: 1  Minutes: 5  Seconds: 43
//
// @author Waylon Bader <wbader@hawaii.edu>
// @date   24 Feb 2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <time.h>
#include <unistd.h>

const int SECONDSINMINUTE = 60;
const int SECONDSINHOUR   = 60 * 60;
const int SECONDSINDAY    = 60 * 60 * 24;
const int SECONDSINYEAR   = 60 * 60 * 24 * 365; // ignoring leap year
const int MINUTESINHOUR   = 60;
const int HOURSINDAY      = 24;
const int DAYSINYEAR      = 365; // ignoring leap year

// const int LEAPDAYSIN42YEARS = 11; 
// Since I have the date hardcoded, I can count the leap days:
// 80, 84, 88, 92, 96, 00, 04, 08, 12, 16, 20
// Switched to an algorithm based leap year tracking

int main() {

   struct tm referenceTime;
   
   referenceTime.tm_sec = 0;
   referenceTime.tm_min = 0;
   referenceTime.tm_hour = 7;
   referenceTime.tm_mday = 22;
   referenceTime.tm_mon = 1;
   referenceTime.tm_year = 1980 - 1900;
   referenceTime.tm_wday = 5;
   
   printf("Reference time:  %s", asctime(&referenceTime));
   
   long elapsedTimeInSeconds;
   int seconds;
   int minutes;
   int hours;
   int days;
   int years;
   
   while(1)
   {
      elapsedTimeInSeconds = (long) difftime(time(NULL), mktime(&referenceTime));
           
      years   = (elapsedTimeInSeconds / SECONDSINYEAR);
      days    = (elapsedTimeInSeconds / SECONDSINDAY - (years / 4 + 1)) % DAYSINYEAR; 
      hours   = (elapsedTimeInSeconds / SECONDSINHOUR) % HOURSINDAY;
      minutes = (elapsedTimeInSeconds / SECONDSINMINUTE) % MINUTESINHOUR;
      seconds = elapsedTimeInSeconds % SECONDSINMINUTE;
      
      printf("Years: %d  Days: %d  Hours: %d  Minutes: %d  Seconds: %d\n", years, days, hours, minutes, seconds);
   
      sleep(1);  
   }
   
   return 0;
}
